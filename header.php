<!DOCTYPE html>
<html lang="fr" dir="ltr" class="js">
  <head>
    <meta charset="utf-8">
    <title> <?php bloginfo( 'name' ); ?></title>
    <?php wp_head(); ?>
  </head>

  <body <?php body_class($custom_classes); ?> >

      <div id="preloader"></div>

    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-4 col-xl-3 block-header">
            <nav class="navbar navbar-expand-lg flex-column">
            <div class="logo">
              <?php
                if ( function_exists( 'the_custom_logo' ) ) {
                  the_custom_logo();
                }
              ?>
            </div>
            <div class="header-content">
              <div class="social-media-m">
                <?php if( get_field('facebook', 'options') ): ?>
                  <a href="<?php the_field('facebook', 'options'); ?>"><i class="fab fa-facebook-square"></i></a>
                <?php endif; ?>
                <?php if( get_field('twitter', 'options') ): ?>
                  <a href="<?php the_field('twitter', 'options'); ?>"><i class="fab fa-twitter"></i></a>
                <?php endif; ?>
                <?php if( get_field('linked_in', 'options') ): ?>
                  <a href="<?php the_field('linked_in', 'options'); ?>"><i class="fab fa-linkedin"></i></a>
                <?php endif; ?>
              </div>
              <div class="infos-m">
                <button type="button" class="btn-modal" data-toggle="modal" data-target="#exampleModalCenter">
                  <i class="fas fa-info-circle"></i>
                </button>
                <!-- Modal -->
                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                  <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle"><?php bloginfo( 'name' ); ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <div class="modal-body">
                        <p class="phone"><b>Téléphone : </b><?php the_field('info_phone', 'option'); ?></p>
                        <p><b>Adresse : </b><?php the_field('info_adress', 'option'); ?></p>
                        <p><b>Mail : </b><?php the_field('info_mail', 'option'); ?></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <button class="navbar-toggler order-3 order-lg-2" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fas fa-align-right"></i>
              </button>

                <?php wp_nav_menu( array(
                  'theme_location'    =>  'primary',
                  'container_class'   =>  'navbar-collapse collapse',
                )
              );
              ?>
              <div class="infos-social">
                <div class="infos">
                  <span class="phone">Tél : <?php the_field('info_phone', 'option'); ?></span>
                  <span><b>Adresse : </b><?php the_field('info_adress', 'option'); ?></span>
                  <span><b>Mail : </b><?php the_field('info_mail', 'option'); ?></span>

                </div>
                <div class="social-media">
                  <?php if( get_field('facebook', 'options') ): ?>
                    <a href="<?php the_field('facebook', 'options'); ?>"><i class="fab fa-facebook-square"></i></a>
                  <?php endif; ?>
                  <?php if( get_field('twitter', 'options') ): ?>
                    <a href="<?php the_field('twitter', 'options'); ?>"><i class="fab fa-twitter"></i></a>
                  <?php endif; ?>
                  <?php if( get_field('linked_in', 'options') ): ?>
                    <a href="<?php the_field('linked_in', 'options'); ?>"><i class="fab fa-linkedin"></i></a>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </nav>
        </div>
