<?php
  // 1. On récupère les infos :
  // $infos_entete = get_field('entete_page');
  // $bandeau_gauche_h1 = get_field('$bandeau_gauche_h1');

  // $infos_h1 = $infos_entete['bloc_de_la_phrase_principale'];

  $infos_h1 = get_field('bloc_de_la_phrase_principale');
  $h1 = $infos_h1['phrase_principale_de_la_page'];
  //Options globales du bloc h1
  $options_h1 = $infos_h1['options_h1'];

    if($options_h1 && in_array('add_para', $options_h1)):
      $add_para = true;
      $under_h1 = $infos_h1['under_h1'];
      $options_spe_under_h1 = $infos_h1['options_spe_under_h1'];
        if($options_spe_under_h1 && in_array('modif_color', $options_spe_under_h1)):
          $change_color_under_h1 = true;
          $couleurs_under_h1 = $infos_h1['couleurs_under_h1'];
        else: $change_color_under_h1 = false;
        endif;
      else: $add_para = false;
      endif;

    if($options_h1 && in_array('add_image', $options_h1)):
      $add_image = true;
      $image_h1 = $infos_h1['image_h1'];
        $image_param = $image_h1['image_bloc'];
        // Paramètres de l'images :
          $image_data = $image_param['image'];
          $options_img = $image_param['options'];

          if($options_img && in_array('limit_height', $options_img)):
            $limit_height = true;
            $height = $image_param['height'];
            $height_mobile = $image_param['height_mobile'];
          else : $limit_height = false;
          endif;

          if($options_img && in_array('add_link_img', $options_img)):
            $add_link_img = true;
            $link_img = $image_param['link_img'];
            $target_blank = $image_param['target_blank'];
          else : $add_link_img = false;
          endif;

          if($options_img && in_array('add_min_width', $options_img)):
            $add_min_width = true;
            $min_width = $image_param['largeur_mini'];
          else : $add_min_width = false;
          endif;

          if($options_img && in_array('img_cover', $options_img)):
            $img_cover = true;
          else: $img_cover = false;
          endif;

      $agencement = $infos_h1['agencement_h1'];
      $alignement_vertical = $infos_h1['alignement_vertical'];
      else: $add_image = false;
      endif;

    if($options_h1 && in_array('add_bandeau_image', $options_h1)):
      $add_bandeau_image = true;
      $bandeau_image = $infos_h1['bloc_images'];
      else: $add_bandeau_image = false;
      endif;

    if($options_h1 && in_array('remove_background', $options_h1)):
      $remove_background = true;
      else: $remove_background = false;
      endif;

    if($options_h1 && in_array('add_shadow', $options_h1)):
      $add_shadow = true;
      else: $add_shadow = false;
      endif;

    if($options_h1 && in_array('change_color_h1', $options_h1)):
      $change_color_bloc_h1 = true;
        $couleurs_bloc_h1 = $infos_h1['bloc_h1'];
      else: $change_color_bloc_h1 = false;
      endif;


    //2. On instancie le container
    ?>
    <section class="container-fluid h1-container <?php echo $agencement.'-h1'; ?> main-section relative <?php if($remove_background): echo ' without-back'; else: echo ' on-bloc border-radius-all'; if($add_shadow): echo ' shadow-container'; endif; endif; ?>">

              <?php
              //IF add image, on place l'image en absolute
              if($add_image):
                if($image_param):
                  //Initialise la div contenu l'image :
                  ?>
                  <div id="container-img-h1" class="container-img-above-h1 show-on-tablet"
                   >
                  <?php
                  if($add_link_img):
                    ?>
                    <a href="<?php echo $link_img;  ?>" <?php if($target_blank): echo 'target="_blank"'; endif; ?>>
                      <?php
                      endif;
                      ?>
                        <img id="img-h1" class="shadow-image border-radius-all <?php if($img_cover): echo 'img-cover'; endif;?>"
                            src="<?php echo $image_data['url'];?>"
                            alt="<?php echo $image_data['alt'];?>"
                        <?php if($limit_height): echo 'style="max-height : '.$height.'px;"'; endif; ?>
                        >
                      <?php
                      if($add_link_img):
                      ?>
                    </a>
                    <?php
                  endif;
                  ?>
                  </div>
                  <?php
                endif;
                 ?>
              <?php
              endif;
              ?>

      <article class="bloc-h1 <?php if($remove_background): echo 'without-back'; endif; ?>"
        <?php if($change_color_bloc_h1): echo 'style="background-color: '.$couleurs_bloc_h1['fond_bloc_h1'].';"'; endif; ?>
        >

        <?php // CONTAINER-H1 ?>
        <div class="container-fluid container-h1 para-content"
        <?php
        if($change_color_bloc_h1): echo 'style="background-color : '.$couleurs_bloc_h1['fond_h1'].'"'; endif;
        ?>
        >
          <div class="row">
            <div class="bloc-center">
            <?php if($add_image): ?>
              <div class="col-sm-6 left"></div>
            <?php endif; ?>
            <h1 class="col-xs-12 <?php if($add_image): echo 'col-sm-6 '; endif; if($change_color_h1): echo 'perso'; endif; ?> "
            <?php if($change_color_bloc_h1): echo 'style="color : '.$couleurs_bloc_h1['texte_h1'].'"'; endif; ?>
            >
              <?php echo $h1; ?>
            </h1>
            <?php if($add_image): ?>
              <div class="col-sm-6 right"></div>
            <?php endif; ?>
            </div>
          </div>

        </div>

        <?php // CONTAINER-UNDER-H1
        if($add_para):
        ?>
        <!-- <div class="padd-bandeau"> -->
        <div class="container-all-under-h1 flex-align-center"
          <?php
          if($change_color_bloc_h1): echo 'style="background-color : '.$couleurs_bloc_h1['fond_under_h1'].'"'; endif;
          ?>>
          <div class="container-fluid container-under-h1 para-content"

          >
            <div class="row bloc-center pad-gutter">
                <?php
                if($add_image): ?>
                  <?php
                  if($image_param):
                    //Initialise la div contenu l'image :
                        if($add_link_img):
                          ?>
                          <a href="<?php echo $link_img;  ?>" <?php if($target_blank): echo 'target="_blank"'; endif; ?>>
                          <?php
                        endif;
                      ?>
                        <img class="hide-on-tablet shadow-image border-radius-all <?php if($img_cover): echo 'img-cover'; endif;?>"
                            src="<?php if($image_data['sizes']['large']): echo $image_data['sizes']['large']; else: echo $image_data['url']; endif;?>"
                            alt="<?php echo $image_data['alt'];?>"
                            style="margin-bottom : 15px; <?php if($limit_height): echo 'max-height : '.$height_mobile.'px;'; endif; ?> "

                        >
                      <?php
                      if($add_link_img):
                        echo '</a>';
                      endif;
                  endif;
                   ?>
                <!-- </div> -->
                <?php endif; ?>

                <?php if($add_image): ?>
                  <div class="col-sm-6 left"></div>
                <?php
                endif;
                ?>
              <div class="col-text col-xs-12 <?php if($add_image): echo 'col-sm-6 '; endif; if($change_color_under_h1): echo 'perso'; endif; ?> "
              <?php if($change_color_bloc_h1): echo 'style="color : '.$couleurs_bloc_h1['texte_under_h1'].'"'; endif; ?>
              >
                <?php echo $under_h1; ?>
              </div>
              <?php if($add_image): ?>
            <div class="col-sm-6 right"></div>
              <?php endif; ?>
            </div>

          </div>
          <?php
          endif;
          ?>

          <?php // CONTAINER bandeau image
          if($add_bandeau_image):

            // TODO: REMETTRE LIMITE HEIGHT OPTION
          ?>

          <div class="container-fluid container-bandeau-image <?php if($change_color_bloc_h1): echo ' color-perso'; endif; ?> "
          <?php
          if($change_color_bloc_h1): echo 'style="background-color : '.$couleurs_bloc_h1['fond_bandeau_image_h1'].'; color : '.$couleurs_bandeau['texte_bandeau_image_h1'].';"'; endif;
          ?>
          >
            <div class="row">
              <div class="bloc-center">
                <?php if($add_image): ?>
                  <div class="col-sm-6 left"></div>
                <?php
                endif;
                ?>
              <div class="no-pad-around col-xs-12 <?php if($add_image): echo 'col-sm-6 '; endif; if($change_color_bloc_h1): echo 'perso'; endif; ?> "
              <?php
              // if($change_color_under_h1): echo 'style="color : '.$couleurs_under_h1['texte'].'"'; endif;
              ?>
              >
                <?php
                add_image_bloc_new($bandeau_image,'div','');
                ?>
              </div>
              <?php if($add_image): ?>
            <div class="col-sm-6 right"></div>
              <?php endif; ?>

            </div>
            </div>

          </div>
          <?php
          endif;
          ?>

        <!-- </div> -->

      </div>
      </article>

    </section>
