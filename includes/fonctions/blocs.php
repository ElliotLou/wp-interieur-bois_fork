<?php
function add_lignes($ligne, $couleurs_texte){
  $texte_ligne = $ligne['texte'];
  $option_police = $ligne['option_police'];
  $options = $ligne['options'];
    if($options && in_array('change_size',$options)):
      $change_size = true;
      $taille_mobile = $ligne['taille_mobile'];
      $taille_desktop = $ligne['taille'];
    else: $change_size = false;
    endif;
    if($options && in_array('police_stylstair',$options)):
      $police_stylstair = true;
      else: $police_stylstair = false;
      endif;
    ?>
    <span class="hide-on-tablet <?php echo $option_police; if($police_stylstair): echo ' font-stylstair'; endif; ?> "
          style=" color : <?php echo $couleurs_texte; ?>;
          <?php  if($change_size): echo 'font-size : '.$taille_mobile.'px;'; endif; ?>
                "
          >
      <?php echo $texte_ligne; ?>
    </span>

    <span class="show-on-tablet <?php echo $option_police; if($police_stylstair): echo ' font-stylstair'; endif;?> "
      style=" color : <?php echo $couleurs_texte; ?>;
      <?php if($change_size): echo 'font-size : '.$taille_desktop.'px'; endif; ?>
            "
          >
      <?php echo $texte_ligne; ?>
    </span>
    <?php
}

function add_button($bouton, $classe){
  // $couleurs_vignettes = get_field('couleurs_bloc_vignettes', 'option');

        $option_bouton = $bouton['options'];
          if($option_bouton && in_array('change_color', $option_bouton)):
            $ctexte = $bouton['couleur_texte'];
            $ctexte_hover = $bouton['couleur_texte_hover'];
            $cfond = $bouton['couleur_fond'];
            $cfond_hover = $bouton['couleur_fond_hover'];
          else :
            $couleur_bouton = get_field('couleurs_boutons','option');
              $ctexte = $couleur_bouton['couleur_texte'];
              $ctexte_hover = $couleur_bouton['couleur_texte_hover'];
              $cfond = $couleur_bouton['couleur_fond'];
              $cfond_hover = $couleur_bouton['couleur_fond_hover'];
          endif;

          if($option_bouton && in_array('add_ombre', $option_bouton)):
            $add_ombre = true;
          else : $add_ombre = false;
          endif;

          if($option_bouton && in_array('target_blank', $option_bouton)):
            $target_blank = true;
          else : $target_blank = false;
          endif;

          $template_dir = get_template_directory_uri();

        $style = $bouton['style_du_bouton'];
        $url_bouton_orange = get_template_directory_uri().'/images/petit_bouton_orange.png';
        $url_bouton_gris = get_template_directory_uri().'/images/petit_bouton_gris.png';

        switch($style):
          case 'defaut':
            $couleurs_bouton = get_field('couleurs_boutons','option');
            $bouton_image = false;
            break;
          case 'perso' :
            $couleurs_bouton = $bouton['couleur_du_bouton']['couleurs_boutons'];
            $bouton_image = false;
            break;
          case 'orange':
            $bouton_image = true;
            break;
          case 'gris':
            $bouton_image = true;
            break;
        endswitch;

        if(!$bouton_image):
          $ctexte = $couleurs_bouton['couleur_texte'];
          $ctexte_hover = $couleurs_bouton['couleur_texte_hover'];
          $cfond = $couleurs_bouton['couleur_fond'];
          $cfond_hover = $couleurs_bouton['couleur_fond_hover'];
        endif;

        $alignement = $bouton['alignement'];
        $police = $bouton['option_police'];
        $label = $bouton['label_du_bouton'];
        $lien = $bouton['lien_btn'];

        //Initialisation de la div contenant le(s) boutons
        ?>
        <div class="boutons-container <?php echo $alignement; ?> ">

          <?php
          if($bouton_image):
            ?>
            <div class="bouton-content bouton-image <?php echo $classe.' '.$style;?>">
              <img class="btn-gris" src="<?php echo $url_bouton_gris; ?>" alt="Bouton">
              <img class="btn-orange" src="<?php echo $url_bouton_orange; ?>" alt="Bouton">

              <a class="police-<?php echo $police; ?>" href="<?php echo $lien; ?>" <?php if($target_blank): echo ' target="_blank"'; endif; ?>>
                <?php echo $label; ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i>
              </a>
            </div>
            <?php
          else:
            ?>
            <div class="bouton-content <?php echo $classe; if($add_ombre): echo ' shadow-vignette'; endif; ?> "

              >
              <a class="police-<?php echo $police; ?>" href="<?php echo $lien; ?>" <?php if($target_blank): echo ' target="_blank"'; endif; ?>
                <?php
                echo '
                style="color : '.$ctexte.'; background-color : '.$cfond.'; "
                onmouseover="this.style.backgroundColor = \''.$cfond_hover.'\'; this.style.color = \''.$ctexte_hover.'\'"
                onmouseout="this.style.backgroundColor = \''.$cfond.'\'; this.style.color = \''.$ctexte.'\'"
                ';
                ?>
                >
                <?php echo $label; ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i>
              </a>
            </div>
            <?php
            endif;
          ?>



        </div>
    <?php
}

function add_titre($param_titre, $balise, $div, $on_bloc, $class){
    // print_r($param_titre);
  $intitule = $param_titre['intitule'];
  $alignement = $param_titre['alignement'];
  $options_titre = $param_titre['options_titre'];

  // Vérifie si couleur modif
  if($options_titre && in_array('modif_color', $options_titre)):
    $modif_couleur_titre_bloc = true;
      $fond_titre = $param_titre['fond_du_titre'];
      $texte_titre = $param_titre['couleur_du_titre'];
  else:
    $modif_couleur_titre_bloc = false;
      $couleurs_globales = get_field('couleurs_globales','option');

        $couleurs_blocs = $couleurs_globales['corps_de_la_page'];

        $fond_titre = $couleurs_blocs['fond_titre_bloc'];
        $texte_titre = $couleurs_blocs['texte_titre_bloc'];
  endif;

    // Vérifie la couleur de la bordure
    if($options_titre && in_array('bordure_titre', $options_titre)):
      $modif_couleur_bordure = true;
    else: $modif_couleur_bordure = false;
    endif;

  ?>

  <<?php echo $div; ?> class="<?php echo $class;?> row"
          <?php if(!$on_bloc): echo 'style="background-color : '.$fond_titre.';"'; endif;?>
  >
    <div class="bloc-center <?php if($on_bloc): echo 'pad-gutter'; else : echo 'pad-thinner'; endif; ?>"
      <?php if($on_bloc):  echo 'style="background-color : '.$fond_titre.';"'; endif;?>
    >

      <<?php echo $balise; ?> class="col-xs-12 titre-container <?php echo $option_police; echo ' '.$alignement; ?>" >
      <?php
      foreach($intitule as $int):
        $ligne = $int['ligne'];
          add_lignes($ligne, $texte_titre);
      endforeach;
      ?>
      </<?php echo $balise; ?>>
        <div class="border-bot align-self-left"
        style=" background-color : <?php if($modif_couleur_bordure): echo $param_titre['couleur_bordure'];
                                          else: $couleurs_globales = get_field('couleurs_globales','option');
                                                $bordure = $couleurs_globales['couleur_bordure'];
                                                echo $bordure;
                                          endif;
                                    ?> ;"></div>

    </div>
  </<?php echo $div; ?>>
  <?php

}

function add_titre_disposition($param_titre, $balise, $div, $on_bloc, $class){
    // print_r($param_titre);
  $intitule = $param_titre['intitule'];
  $alignement = $param_titre['alignement'];
  $options_titre = $param_titre['options_titre'];

  // Vérifie si couleur modif
  if($options_titre && in_array('modif_color', $options_titre)):
    $modif_couleur_titre_bloc = true;
      $texte_titre = $param_titre['couleur_du_titre'];
  // else:
  //   $modif_couleur_titre_bloc = false;
  //     $couleurs_globales = get_field('couleurs_globales','option');
  //
  //       $couleurs_blocs = $couleurs_globales['corps_de_la_page'];
  //
  //       $fond_titre = $couleurs_blocs['fond_titre_bloc'];
  //       $texte_titre = $couleurs_blocs['texte_titre_bloc'];
  endif;


  ?>

  <<?php echo $div; ?> class="<?php echo $class;?> row"
  >
    <div class="bloc-center"
    >

      <<?php echo $balise; ?> class="col-xs-12 titre-container <?php if($modif_couleur_titre_bloc): echo 'color-perso '; endif; echo $option_police; echo ' '.$alignement; ?>"
        <?php if($modif_couleur_titre_bloc): echo 'style="color : '.$texte_titre.'"'; endif; ?>
      >
      <?php
      foreach($intitule as $int):
        $ligne = $int['ligne'];
          add_lignes($ligne, $texte_titre);
      endforeach;
      ?>
      </<?php echo $balise; ?>>

    </div>
  </<?php echo $div; ?>>
  <?php

}

function add_inner_titre($param_titre, $balise, $div, $class){

  $intitule = $param_titre['intitule'];
  $alignement = $param_titre['alignement'];
  $option_police = $param_titre['option_police'];
  $options_titre = $param_titre['options_titre'];


  // Vérifie si couleur perso
  if($options_titre && in_array('modif_color', $options_titre)):
    $modif_couleur_titre_bloc = true;
  else: $modif_couleur_titre_bloc = false;
  endif;

  // Vérifie si fond sur titre
  if($options_titre && in_array('fond_titre', $options_titre)):
    $add_fond_titre = true;
  else: $add_fond_titre = false;
  endif;

  ?>

  <<?php echo $div; ?> class="<?php echo $class; if($add_fond_titre): echo ' art-content'; endif;?>"
          style="<?php if($modif_couleur_titre_bloc): echo 'color : '.$param_titre['couleur_du_titre'].';'; endif; if($add_fond_titre): echo 'background-color : '.$param_titre['fond_du_titre'].';'; endif;?>"
  >
      <<?php echo $balise; ?> class=" titre-container <?php echo $option_police; echo ' '.$alignement;?>" >
        <?php
        foreach($intitule as $int):
          $ligne = $int['ligne'];
            add_lignes($ligne, $param_titre['couleur_du_titre']);
        endforeach;
        ?>
      </<?php echo $balise; ?>>
  </<?php echo $div; ?>>
  <?php

}



function add_image_bloc($image){
  // print_r($image);
  $options = $image['options'];
    if($options && in_array('limit_height', $options)):
      $limit_height = true;
      $height = $image['height'];
    else: $limit_height = false;
    endif;

    if($options && in_array('change_color', $options)):
      $change_color = true;
      $couleurs = $image['couleurs']['couleurs'];
    else: $change_color = false;
    endif;

    if($options && in_array('add_texte', $options)):
      $add_texte = true;
      $textes = $image['texte'];
    else: $add_texte = false;
    endif;


  $images = $image['images'];

    ?>
      <div class="container-fluid <?php if($change_color): echo 'color-perso'; endif; ?> "
      <?php if($change_color): echo 'style="background-color : '.$couleurs['fond'].'; color : '.$couleurs['texte'].';"'; endif; ?>
      >
        <div class="row">
          <?php if($textes): ?>
          <div class="col-xs-12 pad-around-small p-no-marge">
            <?php
            foreach($textes as $text):
              $ligne = $text['ligne'];
              add_lignes($ligne, $couleurs['texte']);
            endforeach;
            ?>
          </div>
          <?php
        endif;


          foreach($images as $img):
            switch($img['acf_fc_layout']):
              case 'image':
              $nb_img = count($images);
              switch($nb_img):
                case '1' : $class_col = 'align-center no-pad col-xs-12';
                  break;
                case '2' : $class_col = 'align-center no-pad col-xs-12 col-sm-6';
                  break;
                case '3' : $class_col = 'align-center no-pad col-xs-6 col-sm-4';
                  break;
                case '4' : $class_col = 'align-center no-pad col-xs-6 col-sm-4 col-md-3';
                  break;
                case '5' : $class_col = 'align-center no-pad col-xs-6 col-sm-4 col-md-5';
                  break;
              endswitch;

                $options_img = $img['options'];
                if($options_img && in_array('add_link_img', $options_img)):
                  $add_link_img = true;
                  $link_img = $img['link_img'];
                    $target_blank = $img['target_blank'];
                else: $add_link_img = false;
                endif;

                if($options_img && in_array('img_cover', $options_img)):
                  $img_cover = true;
                else: $img_cover = false;
                endif;

                $img_prop = $img['image'];
                ?>
                <div class="<?php echo $class_col; ?> ">
                  <div class="img-container justify-center" >
                  <?php
                  if($add_link_img):
                    ?>
                    <a href="<?php echo $link_img; ?>" <?php if($target_blank): echo 'target="_blank"'; endif; ?>>
                    <?php
                  endif;
                  ?>

                    <img src="<?php echo $img_prop['url']; ?> " alt="<?php echo $img_prop['alt']; ?>"
                      <?php if($limit_height): echo 'style="max-height : '.$height.'px"'; endif; ?>
                      <?php if($img_cover): echo 'class="img-cover"'; endif; ?>
                    >
                    <?php
                    if($add_link_img):
                      ?>
                    </a>
                      <?php
                    endif;
                    ?>
                  </div>
                </div>
                <?php
                break;
              case 'images_avant_apres':
                $nb_img = count($images);
                switch($nb_img):
                  case '1' : $class_col = 'align-center row-1';
                    break;
                  case '2' : $class_col = 'align-center row-2';
                    break;
                  case '3' : $class_col = 'align-center row-3';
                    break;
                  case '4' : $class_col = 'align-center row-4';
                    break;
                  case '5' : $class_col = 'align-center row-5';
                    break;
                endswitch;
                ?>
                <div class="<?php echo $class_col; ?> photo-avant-apres">
                  <div class="item-photo">

                    <div class="img-container juxtapose-container justify-center"
                    <?php
                    // if($limit_height): echo 'style="max-height : '.$height.'px"'; endif;
                    ?>
                    >
                    <?php
                    $image_avant = $img['image_avant'];
                    $image_apres = $img['image_apres'];
                    ?>
                      <div class="juxtapose">
                        <img src="<?php echo $image_avant['url']; ?>" data-label="Avant"/>
                        <img src="<?php echo $image_apres['url']; ?>" data-label="Après"/>
                      </div>
                    <?php
                    // echo do_shortcode('[sciba leftsrc="'.$image_avant['url'].'" leftlabel="Avant" rightsrc="'.$image_apres['url'].'" rightlabel="Après" mode="horizontal" width="100%"]');
                    ?>
                  </div>
                  </div>
                </div>
                <?php
                break;
            endswitch;



          endforeach;
          ?>
        </div>
      </div>
    <?php

}

function add_image_bloc_without_back($image){
  // print_r($image);
  $options = $image['options'];
    if($options && in_array('limit_height', $options)):
      $limit_height = true;
      $height = $image['height'];
    else: $limit_height = false;
    endif;

    if($options && in_array('change_color', $options)):
      $change_color = true;
      $couleurs = $image['couleurs']['couleurs'];
    else: $change_color = false;
    endif;

    if($options && in_array('add_texte', $options)):
      $add_texte = true;
      $textes = $image['texte'];
    else: $add_texte = false;
    endif;


    $images = $image['images'];

    ?>
      <div class="container-fluid <?php if($change_color): echo 'color-perso'; endif; ?> "
      <?php if($change_color): echo 'style="background-color : '.$couleurs['fond'].'; color : '.$couleurs['texte'].';"'; endif; ?>
      >
        <div class="row">
          <?php if($textes): ?>
          <div class="col-xs-12 pad-around-small p-no-marge">
            <?php
            foreach($textes as $text):
              $ligne = $text['ligne'];
              add_lignes($ligne, $couleurs['texte']);
            endforeach;
            ?>
          </div>
          <?php
        endif;

          $nb_img = count($images);
          switch($nb_img):
            case '1' : $class_col = 'align-center no-pad col-xs-12';
              break;
            case '2' : $class_col = 'align-center no-pad col-xs-12 col-sm-6';
              break;
            case '3' : $class_col = 'align-center no-pad col-xs-6 col-sm-4';
              break;
            case '4' : $class_col = 'align-center no-pad col-xs-6 col-sm-4 col-md-3';
              break;
            case '5' : $class_col = 'align-center no-pad col-xs-6 col-sm-4 col-md-5';
              break;
          endswitch;
          foreach($images as $img):
            switch($img['acf_fc_layout']):
              case 'image':
                $options_img = $img['options'];
                if($options_img && in_array('add_link_img', $options_img)):
                  $add_link_img = true;
                  $link_img = $img['link_img'];
                    $target_blank = $img['target_blank'];
                else: $add_link_img = false;
                endif;

                if($options_img && in_array('img_cover', $options_img)):
                  $img_cover = true;
                else: $img_cover = false;
                endif;

                $img_prop = $img['image'];
                ?>
                <div class="<?php echo $class_col; ?>">
                  <div class="img-container justify-center" >
                  <?php
                  if($add_link_img):
                    ?>
                    <a href="<?php echo $link_img; ?>" <?php if($target_blank): echo 'target="_blank"'; endif; ?>>
                    <?php
                  endif;
                  ?>

                    <img src="<?php echo $img_prop['url']; ?> " alt="<?php echo $img_prop['alt']; ?>"
                    <?php if($limit_height): echo 'style="max-height : '.$height.'px"'; endif; ?>
                    >
                    <?php
                    if($add_link_img):
                      ?>
                    </a>
                      <?php
                    endif;
                    ?>
                  </div>
                </div>
                <?php
                break;
            endswitch;



          endforeach;
          ?>
        </div>
      </div>
    <?php

}

function add_para($paragraphe, $bloc_has_titre, $bloc_color_change, $couleurs_du_bloc, $on_bloc){

  if($paragraphe):
    $para_options = $paragraphe['options'];

      if($para_options && in_array('change_color', $para_options)):
        $change_color = true;
        $couleurs_perso = $paragraphe['couleurs']['couleurs'];
      else: $change_color = false;
      endif;
    //1. Initialise l'article paragraphe :
    ?>
    <article class="art-paragraphe <?php if($bloc_color_change || $change_color): echo "color-perso"; endif; ?>"
      <?php
        if($change_color):
          echo 'style="color : '.$couleurs_perso['texte'].'; background-color : '.$couleurs_perso['fond'].'"';
        else:
          if($bloc_color_change): echo 'style="color : '.$couleurs_du_bloc['couleur_texte_inside'].'; background-color : '.$couleurs_du_bloc['couleur_fond_inside'].';"'; endif;
        endif;
      ?>
    >
      <?php

      if($para_options && in_array('add_titre', $para_options)):
        $class_titre = 'titre';
        $article_has_titre = true;
        $titre_paragraphe = $paragraphe['titre_paragraphe'];
        $param_titre = $titre_paragraphe['parametres_titre_bloc'];

        if($bloc_has_titre):
          $balise = 'h3';
          $class_titre = 'inner-titre';
          add_titre($param_titre, $balise, 'header', $on_bloc, $class_titre);
        else:
          $balise = 'h2';
          add_titre($param_titre, $balise, 'header', $on_bloc, $class_titre);
        endif;


      endif;

      $contenu_paragraphe = $paragraphe['texte_paragraphe'];

      //2. Initialisation du paragraphe
      ?>
      <!-- <div class="para-container inner-bloc">  -->
        <main class="para-content pad-gutter container-fluid <?php if($add_titre): echo 'art-content'; endif; ?>">
          <div class="bloc-center">
          <?php echo $contenu_paragraphe; ?>
          </div>
        </main>
      <!-- </div> -->
    </article>
    <?php
  endif;// $paragraphe

}

function add_para_new_colonne($paragraphe, $bloc_has_titre, $bloc_color_change, $couleurs_du_bloc, $on_bloc){

  if($paragraphe):
    $para_options = $paragraphe['options'];

      if($para_options && in_array('change_color', $para_options)):
        $change_color = true;
        $couleurs_perso = $paragraphe['couleurs']['couleurs'];
      else: $change_color = false;
      endif;
    //1. Initialise l'article paragraphe :
    ?>
    <article class="art-paragraphe <?php if($bloc_color_change || $change_color): echo "color-perso"; endif; ?>"
      <?php
        if($change_color):
          echo 'style="color : '.$couleurs_perso['texte'].'; background-color : '.$couleurs_perso['fond'].'"';
        else:
          if($bloc_color_change): echo 'style="color : '.$couleurs_du_bloc['couleur_texte_inside'].'; background-color : '.$couleurs_du_bloc['couleur_fond_inside'].';"'; endif;
        endif;
      ?>
    >
      <?php

      // print_r($paragraphe['titre_paragraphe']);

      if($para_options && in_array('add_titre', $para_options)):
        $class_titre = 'titre';
        $article_has_titre = true;
        $titre_paragraphe = $paragraphe['titre_paragraphe'];
        $param_titre = $titre_paragraphe['parametres_titre_disposition'];

        if($bloc_has_titre):
          $balise = 'h3';
          $class_titre = 'inner-titre';
          add_titre_disposition($param_titre, $balise, 'header', $on_bloc, $class_titre);
        else:
          $balise = 'h2';
          add_titre_disposition($param_titre, $balise, 'header', $on_bloc, $class_titre);
        endif;


      endif;

      $contenu_paragraphe = $paragraphe['texte_paragraphe'];

      //2. Initialisation du paragraphe
      ?>
      <!-- <div class="para-container inner-bloc">  -->
        <main class="para-content pad-gutter container-fluid <?php if($add_titre): echo 'art-content'; endif; ?>">
          <div class="bloc-center">
          <?php echo $contenu_paragraphe; ?>
          </div>
        </main>
      <!-- </div> -->
    </article>
    <?php
  endif;// $paragraphe

}


function add_bloc_vignettes($labels_info){
  // print_r($labels_info);
  //Variables =
  $couleurs_globales = get_field('couleurs_globales','option');
  $couleurs_vignettes = get_field('couleurs_bloc_vignettes', 'option');

  $labels_info = $labels_info['bloc_vignettes'];
  $bloc_options = $labels_info['options'];


    if($bloc_options && in_array('remove_background', $bloc_options)):
      $on_bloc = false;
      $remove_background = true;
    else: $on_bloc = true; $remove_background = false;
    endif;

    if($bloc_options && in_array('add_shadow', $bloc_options)):
      $add_shadow = true;
    else: $add_shadow = false;
    endif;

  ?>

  <section class="main-section bloc-vignettes container-fluid <?php if($on_bloc): echo ' on-bloc border-radius-all'; if($add_shadow): echo ' shadow-container'; endif; else: echo ' without-bloc'; endif; ?> ">
    <?php
    // On vérifie si il y a un titre et on récupères les paramètres
    if($bloc_options && in_array('add_titre', $bloc_options)):
      $add_titre = true;
      $titre_infos = $labels_info['titre'];
      $param_titre = $titre_infos['parametres_titre_bloc'];

    else : $add_titre = false;
    endif;

    if($add_titre):
      $vignette_has_titre = true;
      add_titre($param_titre, 'h2', 'header',$on_bloc, 'titre titre-bloc');
    else: $vignette_has_titre = false;
    endif;

    //On vérifie si il y a un paragraphe et on initialise la première section
    if($bloc_options && in_array('add_para', $bloc_options)):
      $paragraphe = $labels_info['paragraphe_before'];
      $para_infos = $paragraphe['paragraphe'];

      add_para($para_infos, $vignette_has_titre, false, null, $on_bloc);

    endif;

    //On récupère les info sur les vignettes et on instancie la section

    $vignettes = $labels_info['vignettes'];
    $back_vignettes = $vignettes['fond_vignettes_bloc_vignettes'];
    //Instanciation du container de vignette_has_titre
    ?>
      <!-- <section> -->
        <article class="vignettes-container container-fluid"
        <?php
        switch($back_vignettes):
          case 'couleur_defaut' :
            echo 'style=" background-color : '.$couleurs_globales['couleur_fond_principal'].';"';
            break;
          case 'couleur_change':
            echo 'style=" background-color : '.$vignettes['couleur_derriere_vignettes'].';"';
            break;
          case 'image' :
            echo 'style=" background : url('.$vignettes['image_derriere_vignettes']['url'].') center center fixed;
                  -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;
                  background-size: cover;
                "';
        endswitch;
        ?>
        >

        <?php
        $type_vignette = $vignettes['type_vignette'];
        switch($type_vignette):
          case 'textes':
            $vignettes_texte = $vignettes['vignettes_texte'];
            add_vignettes_textes($vignettes_texte);
          break;

          case 'image' :
            $vignette_image = $vignettes['vignettes_image'];
            $couleurs_vignette = get_field('couleurs_bloc_vignettes','option');
            add_vignettes_image($vignette_image);
          break;

          case 'titre_para':
            $vignettes_colonne = $vignettes['vignettes_colonne'];
            $options_vignette = $vignettes_colonne['options'];
            if($options_vignette):
              if(in_array('change_color', $options_vignette)):
                $fond_entete = $vignettes_colonne['fond_entete'];
                $textes_entete = $vignettes_colonne['textes_entete'];
                $fond_para = $vignettes_colonne['fond_para'];
                $textes_para = $vignettes_colonne['textes_para'];

              else:
                $couleurs_vignettes_colonne = $couleurs_vignettes['vignettes_colonne'];
                $fond_entete = $couleurs_vignettes_colonne['fond_entete'];
                $textes_entete = $couleurs_vignettes_colonne['texte_entete'];
                $fond_para = $couleurs_vignettes_colonne['fond_para'];
                $textes_para = $couleurs_vignettes_colonne['texte_para'];
              endif;

              if(in_array('border_radius', $options_vignette)):
                $bordered = true;
                $borders = $vignettes_colonne['coins_arrondies'];
              else: $bordered = false;
              endif;

              if(in_array('add_ombre', $options_vignette)):
                $shadow = true;
              else: $shadow = false;
              endif;
            endif;


            $vignettes = $vignettes_colonne['vignettes'];
            //initialisation du container
            ?>
            <div class="container-fluid bloc-center container-vcolonnes">
              <div class="row flex-center">
            <?php
              foreach($vignettes as $vignette):
                $opt_vignette = $vignette['options'];
                if($opt_vignette && in_array('add_link', $opt_vignette)):
                  $add_link = true;
                  $link = $vignette['lien'];
                else: $add_link = false;
                endif;

                $entete = $vignette['entete'];
                $paragraphe = $vignette['paragraphe'];
                //Initialisation de la vignette colonne
                ?>
                <div class="vignette-colonne col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xlg-5">
                  <div class="inner-vignette <?php if($bordered): echo " ".$borders; endif; if($shadow): echo ' shadow-vignette'; endif;?>">
                    <div
                      class="entete-vignette"
                      style="background-color : <?php echo $fond_entete; ?> "
                    >
                      <?php
                        foreach($entete as $ent):
                          $ligne = $ent['ligne'];
                            add_lignes($ligne, $textes_entete);
                        endforeach;
                      ?>
                    </div>
                    <div
                      class="para-vignette"
                      style="background-color : <?php echo $fond_para; ?>; color : <?php echo $textes_para; ?>"
                    >
                      <?php echo $paragraphe; ?>
                    </div>
                    <?php
                      if($add_link):
                        ?>
                        <div class="learn-more">
                          <a href=" <?php echo $link; ?> "> ... En savoir plus </a>
                        </div>
                        <?php
                      endif;
                    ?>
                  </div>
                </div>
                <?php
              endforeach;
              ?>
              </div>
            </div>
              <?php

          break;
        endswitch;

        //Bloc boutons
        if($bloc_options && in_array('add_bouton', $bloc_options)):
          $bouton_bloc = $labels_info['bouton'];

          $bouton = $bouton_bloc['parametres_du_bouton'];

          add_button($bouton, 'bouton-vignette');
        endif;
        ?>
      </article>
      <!-- </section> -->

    <?php

    //On vérifie si il y a un paragraphe et on initialise la première section
    if($bloc_options && in_array('add_para_after', $bloc_options)):
      $paragraphe = $labels_info['paragraphe_after'];
      $para_infos = $paragraphe['paragraphe'];

      add_para($para_infos, $vignette_has_titre, false, null, $on_bloc);

    endif;
    ?>
  </section>
  <?php
}

function add_vignettes_image($vignette_image){
  $couleurs_vignettes = get_field('couleurs_bloc_vignettes', 'option');

  $style_vignette_image = $vignette_image['style_vignette'];

  $vimage_options = $vignette_image['options'];
    if($vimage_options && in_array('change_color', $vimage_options)):
      $couleur_nav = $vignette_image['couleur_nav_icone'];
      $fond_vignettes = $vignette_image['fond_vignettes'];
      $couleur_labels_vignettes = $vignette_image['labels_vignettes'];

      else:
      $couleurs_vignettes_image = $couleurs_vignettes['vignettes_images'];

      $couleur_nav = $couleurs_vignettes_image['couleur_nav_icone'];
      $fond_vignettes = $couleurs_vignettes_image['couleur_fond_vignettes'];
      $couleur_labels_vignettes = $couleurs_vignettes_image['couleur_label'];
      endif;

    if($vimage_options && in_array('rect', $vimage_options)):
      $rect = true;
        $forme_rect = $vignette_image['forme_rectangle'];
      else:
      $rect = false;
      endif;

    if($vimage_options && in_array('gallery', $vimage_options)):
      $gallery = true;
      else:
      $gallery = false;
      endif;


    if($vimage_options && in_array('add_ombre', $vimage_options)):
      $add_ombre = true;
      else: $add_ombre = false;
      endif;

    if($vimage_options && in_array('border_radius', $vimage_options)):
      $bordered = true;
      $borders = $vignette_image['coins_arrondies'];
      else: $bordered = false;
      endif;


  $vignettes = $vignette_image['vignettes'];
  $dispo_vignettes = $vignette_image['disposition_des_vignettes'];
  $nb_vignettes = $vignette_image['nb_vignettes'];


  $fond_label_cover_rgb = hex2rgb($fond_vignettes);
  $fond_label_cover_rgb = implode(",", $fond_label_cover_rgb);
  $fond_label_cover = 'rgba('.$fond_label_cover_rgb.', .6)';

  ?>


    <?php
      if($dispo_vignettes == 'one_line'):
        ?>
        <div class="slider <?php if($gallery): echo 'gallery'; endif; ?> bloc-center" data-slide="<?php echo $nb_vignettes;?>" data-slideold="<?php echo $nb_vignettes;?>" data-color="<?php echo $couleur_nav;?>">
         <?php
      else:
        ?>
        <div class="slider <?php if($gallery): echo 'gallery'; endif; ?> bloc-center multiligne" data-slide="<?php echo $nb_vignettes;?>" data-slideold="<?php echo $nb_vignettes;?>" data-color="<?php echo $couleur_nav;?>">
         <?php
      endif;

           foreach($vignettes as $vignette):

             $image = $vignette['image'];
             $options = $vignette['options'];

             if($options && in_array('add_link', $options)):
               $add_link = true;
             else: $add_link = false;
             endif;

             ?>

             <div class="vignette">
                <div class=" <?php if($rect): echo $forme_rect; else: echo 'item-carre'; endif; ?>">
                  <?php
                   if($add_link): echo '<a class="link-vignette" href="'.$vignette['lien'].'" target="_blank">'; endif;
                   if($gallery): echo '<a href="'.$image['url'].'">'; endif;
                  ?>

                    <?php
                    switch ($style_vignette_image):
                      case 'img_cover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                        </div>
                        <?php
                        break;
                      case 'img_cover_texte_hover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <?php
                          $textes_hover = $vignette['text_hover'];
                          ?>
                          <div class="text-hover-full"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                              foreach ($textes_hover as $lignes):
                                $ligne = $lignes['ligne'];
                                  add_lignes($ligne, $couleur_labels_vignettes);
                                endforeach;
                            ?>
                          </div>
                        </div>
                        <?php
                        break;
                      case 'img_cover_zoom_hover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="zoom-hover-full"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <i class="fa fa-search-plus fa-4x" aria-hidden="true" style="color : <?php echo $couleur_labels_vignettes;?> "></i>
                          </div>
                        </div>
                        <?php
                        break;

                      case 'img_cover_et_label':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="label-top"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                              $label = $vignette['label'];
                            ?>
                            <p class="police-bolder"
                              style="color : <?php echo $couleur_labels_vignettes; ?>"
                            >
                              <?php echo $label; ?>
                            </p>
                          </div>

                        </div>
                        <?php
                        break;
                      case 'img_cover_et_label_texte_hover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="label-top"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                              $label = $vignette['label'];
                            ?>
                            <p class="police-bolder"
                              style="color : <?php echo $couleur_labels_vignettes; ?>"
                            >
                              <?php echo $label; ?>
                            </p>
                          </div>
                          <div class="text-more-hover"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                            $textes_hover = $vignette['text_hover'];
                            foreach ($textes_hover as $lignes):
                              $ligne = $lignes['ligne'];
                                add_lignes($ligne, $couleur_labels_vignettes);
                              endforeach;
                            ?>
                          </div>

                        </div>
                        <?php
                        break;
                      case 'img_cover_et_label_zoom_hover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="label-top"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                              $label = $vignette['label'];
                            ?>
                            <p class="police-bolder"
                              style="color : <?php echo $couleur_labels_vignettes; ?>"
                            >
                              <?php echo $label; ?>
                            </p>
                          </div>

                          <div class="zoom-hover-more"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <i class="fa fa-search-plus fa-4x" aria-hidden="true" style="color : <?php echo $couleur_labels_vignettes;?> "></i>
                          </div>

                        </div>
                        <?php
                        break;
                      case 'img_et_label':
                        ?>
                        <div class="slide-content <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="label-container-under">
                            <p class="police-bolder"
                              style="color : <?php echo $couleur_labels_vignettes;?>"
                            >
                              <?php echo $vignette['label']; ?>
                            </p>
                          </div>

                        </div>
                        <?php
                        break;
                      default:
                        # code...
                        break;
                    endswitch;
                    ?>

                    <!-- <div class="slide-content<?php if($add_label): echo ' with-label'; endif; if($cover): echo " cover"; endif; if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; if($with_zoom): echo ' with-zoom'; endif;?>"
                         style="background-color : <?php echo $fond_vignettes;?>;"
                      >
                      <div class="img-container-vignette">
                        <div class="img-content">
                          <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                        </div>
                      </div>
                      <?php
                      if($add_label || $with_zoom):
                        ?>
                        <div class="label-container <?php if($add_link && $cover): echo 'covering'; endif; if($with_text):echo ' with-text'; endif;?>"
                          <?php if($with_zoom): echo 'style="background-color : '.$fond_label_cover.';"';endif; ?>
                          >
                          <?php

                          if($with_zoom):
                            ?>
                            <i class="fa fa-search-plus" aria-hidden="true" style="color : <?php echo $couleur_labels_vignettes;?> "></i>
                            <?php
                          elseif($add_label && $label_hover):
                            ?>
                            <p class="justify-center">
                              <?php echo $vignette['label']; ?>
                            </p>
                            <?php
                          else:
                            ?>
                            <p class="label-vignette " style="<?php if($cover): echo '; background-color : '.$fond_label_cover.';'; else: echo ';'; endif;?>">
                              <span class="label-perso"
                                style="color : <?php echo $couleur_labels_vignettes;?>"
                              >
                                <?php echo $vignette['label']; ?>
                              </span>
                            </p>
                            <?php
                            if($with_text):
                              $text_to_display = $vignette['text_hover'];
                              ?>
                              <div class="label-more" style="background-color : <?php echo $fond_label_cover; ?>; color : <?php echo $couleur_labels_vignettes;?>; ">
                                <?php
                                foreach($text_to_display as $text):
                                  $ligne = $text['ligne'];
                                  $texte = $ligne['texte'];
                                  $option_police = $ligne['option_police'];
                                  $options = $ligne['options'];
                                    if($options && in_array('change_size', $options)):
                                      $change_size = true;
                                      $size = $text['taille'];
                                    else : $change_size = false;
                                    endif;
                                    ?>
                                    <p class="<?php echo $option_police; ?>"
                                      <?php if($change_size): echo 'style="font-size : '.$size.'";'; endif; ?>
                                    >
                                      <?php echo $texte; ?>
                                    </p>
                                    <?php
                                endforeach;
                                ?>
                              </div>
                              <?php
                            endif;
                          endif;
                          ?>



                        </div>

                        <?php
                      endif;
                      ?>
                    </div> -->

                  <?php
                  if($add_link || $gallery): echo '</a>'; endif;
              ?>
              </div>
            </div>
              <?php
           endforeach;
         ?>
       </div>
        <?php
}

function add_vignettes_textes($vignettes_texte){
  $couleurs_vignettes = get_field('couleurs_bloc_vignettes','option');
    ?>
    <div class="container-fluid bloc-center container-vtextes">
      <div class="row flex-center">

    <?php
      $vtexte_options = $vignettes_texte['options'];
      if($vtexte_options):
        if(in_array('add_ombre', $vtexte_options)):
          $add_ombre = true;
        else: $add_ombre = false;
        endif;

        if(in_array('change_color',$vtexte_options)):
          $couleur_labels_vignettes = $vignettes_texte['labels_vignettes'];
          $fond_vignettes = $vignettes_texte['fond_vignettes'];
          $fond_vignettes_hover = $vignettes_texte['fond_vignettes_hover'];
          $couleur_labels_hover = $vignettes_texte['labels_vignettes_hover'];
        else:
          $couleurs_vignettes_texte = $couleurs_vignettes['vignettes_textes'];
            $couleur_labels_vignettes = $couleurs_vignettes_texte['couleur_texte'];
            $fond_vignettes = $couleurs_vignettes_texte['couleur_fond'];
            $fond_vignettes_hover = $couleurs_vignettes_texte['couleur_fond_hover'];
            $couleur_labels_hover = $couleurs_vignettes_texte['couleur_texte_hover'];
        endif;

        if(in_array('border_radius', $vtexte_options)):
          $bordered = true;
          $borders = $vignettes_texte['coins_arrondies'];
        else: $bordered = false;
        endif;
      endif;


    $vtexte_vignettes = $vignettes_texte['vignettes'];
          if($vtexte_vignettes):
            foreach($vtexte_vignettes as $vignette):
              $label = $vignette['label'];
              $options = $vignette['options'];
              if($options && in_array('add_link',$options)):
                $add_link = true;
              else: $add_link = false;
              endif;
              ?>
              <div class=" col-xs-12 col-sm-6 col-md-4 col-lg-3"
              >
                   <?php
                   if($add_link):
                     echo '
                      <a class="vignette-texte ';
                      if($add_ombre): echo 'shadow ';endif; if($bordered): echo ' '.$borders.' '; endif;
                      echo 'link-vignette" href="'.$vignette['lien'].'" target="_blank"
                        style="color : '.$couleur_labels_vignettes.'; background-color : '.$fond_vignettes.'; "
                        onmouseover="this.style.backgroundColor = \''.$fond_vignettes_hover.'\'; this.style.color = \''.$couleur_labels_hover.'\'"
                        onmouseout="this.style.backgroundColor = \''.$fond_vignettes.'\'; this.style.color = \''.$couleur_labels_vignettes.'\'"
                      >';
                      foreach($label as $ligne):
                        ?>
                        <span>
                          <?php echo $ligne['ligne']; ?>
                        </span>
                        <?php
                      endforeach;
                    echo' </a>
                      ';
                   else:
                     ?>
                       <div class="vignette-texte <?php if($add_ombre): echo "shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?>"
                            style="background-color : <?php echo $fond_vignettes;?>;">

                             <?php
                             foreach($label as $ligne):
                               ?>
                               <span style="color : <?php echo $couleur_labels_vignettes; ?>; ">
                                 <?php echo $ligne['ligne']; ?>
                               </span>
                               <?php
                             endforeach;
                             ?>
                       </div>
                           <?php

                   endif;
               echo '</div>';

            endforeach;
          endif;
          ?>
        </div>
      </div>
          <?php
}

function add_bouton_colonne($contenu_colonne, $couleurs_colonnes, $url_bouton, $nb_col_tablette,$nb_col_desktop){

  $couleurs_col_bouton = $couleurs_colonnes['colonne_bouton'];
  $link_col = $contenu_colonne['lien'];
  $contenu_col = $contenu_colonne['contenu'];


  ?>

  <div <?php echo 'onclick="window.location.href=\''.$link_col.'\'"'; ?>
  class="show-on-tablet col-bouton clickable  col-content col-mob-1 col-tab-<?php echo $nb_col_tablette; ?> col-desk-<?php echo $nb_col_desktop; ?>">

      <div class="inner-col">
        <img class="bouton-colonne" src="<?php echo $url_bouton; ?> " alt="bouton clickable">

        <?php
        foreach($contenu_col as $content):
          $ligne = $content['ligne'];
            add_lignes($ligne, '#FFFFFF');
        endforeach;
        ?>
      </div>
  </div>

  <div class="container-fluid hide-on-tablet">
    <div class="boutons-container under-col flex-center ">
    <?php
    $url_bouton_orange = get_template_directory_uri().'/images/petit_bouton_orange.png';
    $url_bouton_gris = get_template_directory_uri().'/images/petit_bouton_gris.png';
    ?>

        <div class="bouton-content vertical orange bouton-image <?php echo $classe.' '.$style;?>">
          <img class="btn-gris" src="<?php echo $url_bouton_gris; ?>" alt="Bouton">
          <img class="btn-orange" src="<?php echo $url_bouton_orange; ?>" alt="Bouton">

          <a class="police-bold" href="<?php echo $link_col; ?>">
            <?php
            foreach($contenu_col as $content):
              $ligne = $content['ligne'];
                add_lignes($ligne, '#FFFFFF');
            endforeach;
            ?>
          </a>
        </div>
    </div>
  </div>

  <?php
}

?>
