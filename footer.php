
  <hr>

  <footer>
    <div class="infos">
        <p class="phone"><b></b><i class="fas fa-phone"></i> : <?php the_field('info_phone', 'option'); ?></p>
        <p><b>Adresse :</b> <?php the_field('info_adress', 'option'); ?></p>
        <p><b>Mail :</b> <?php the_field('info_mail', 'option'); ?></p>
        <p>Copyright © <b>Intérieur Bois</b> - <a href="#">Copyright</a> - <a href="#">Mentions Légales</a> </p>
    </div>
    <div class="gdh">
      <p>Site réalisé par : </p>
      <div class="row">
        <div class="col-4"></div>
        <div class="col-2">
          <img src="/ib_bois/wp-content/uploads/2018/04/LogoHabitat.png" alt="">
        </div>
        <div class="col-2">
          <img src="/ib_bois/wp-content/uploads/2018/04/Logo_GHDCOM_EXE.png" alt="">
        </div>
        <div class="col-4"></div>
      </div>
    </div>
  </footer>
  <?php wp_footer(); ?>

  </body>
</html>
