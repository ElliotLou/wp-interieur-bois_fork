<?php

// CUSTOM THEME SUPPORT

    // THUMBNAIL
    add_theme_support('post-thumbnails');

    // THUMBNAIL SIZE
    add_image_size( 'custom-size', 300 , 200, array( 'left', 'top' ) );
    add_image_size( 'slider-size', 3000 , 1600 , array( 'left', 'top' ) );

    // LOGO
    add_theme_support( 'custom-logo' );

    // BACKGROUND
    add_theme_support('custom-background');

    // HEADER
    add_theme_support('custom-header');

    // POST FORMAT
    add_theme_support('post-format');


// ACF PAGE BUILDER
function support_acfpb( $support = array() ) {

    $support['post_type'] = array(
        'page',
        'post'
    );

    $support['page_template'] = array(
        'page-templates/page-sectioned.php'
    );

    return $support;

}
add_filter('acfpb_set_locations', 'support_acfpb', 10, 1);


// STYLES & SCRIPTS
function init_styles_and_scripts() {

  // CSS //

  wp_enqueue_style(
    'bs-css',
    get_template_directory_uri() . '/node_modules/bootstrap/dist/css/bootstrap.min.css',
    array(),
    '4.0.0',
    'all'
  );

  wp_enqueue_style(
    'bxslider-css',
    get_template_directory_uri() . '/node_modules/bxslider/dist/jquery.bxslider.css',
    array(),
    '4.2.12',
    'all'
  );

  wp_enqueue_style(
    'theme-css',
    get_template_directory_uri() . '/css/main.css',
    array(),
    '1.0.0',
    'all'
  );


  // JS //

  wp_enqueue_script(
      'jquery'
  );

  wp_enqueue_script(
    'theme-js',
    get_template_directory_uri() . '/js/main.js',
    array(),
    '1.0.0',
    false
  );

  wp_enqueue_script(
    'google-js',
    get_template_directory_uri() . '/js/google.js',
    array(),
    '1.0.0',
    true
  );

  wp_enqueue_script(
    'google-map',
    'https://maps.googleapis.com/maps/api/js?key=AIzaSyBOYwuRvoIVZinZmJeQrwEnHTz4mm53cK0',
    array(),
    '3',
    true
  );

  wp_enqueue_script(
    'popper',
    'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
    array(),
    '1.12.9',
    true
  );



  wp_enqueue_script(
    'bs-js',
    get_template_directory_uri() . '/node_modules/bootstrap/dist/js/bootstrap.min.js',
    array(),
    '4.0.0',
    true
  );

  wp_enqueue_script(
    'font-awesome',
    'https://use.fontawesome.com/releases/v5.0.9/js/all.js',
    array(),
    '5.0.9',
    true
  );

  wp_enqueue_script(
    'bxslider-js',
    get_template_directory_uri() . '/node_modules/bxslider/dist/jquery.bxslider.min.js',
    array(),
    '4.2.12',
    true
  );

}
add_action('wp_enqueue_scripts', 'init_styles_and_scripts');





// CUSTOM MENUS
function custom_menus_setup() {
  add_theme_support('menus');
  register_nav_menu('primary', 'Header Navigation');
  register_nav_menu('secondary', 'Footer Navigation');
}
add_action('init', 'custom_menus_setup');





// ENLÈVE LES ATTRIBUTS DE PAGE POUR LES UTILISTEURS < ADMIN
function remove_page_attribute_meta_box() {
    if( is_admin() ) {
        if( current_user_can('editor') ) {
            remove_meta_box('pageparentdiv', 'page', 'normal');
        }
    }
}
add_action( 'admin_menu', 'remove_page_attribute_meta_box' );





// PRODUITS
function my_acf_init() {

  // CLÉ API GOOGLE MAPS / REQUIS POUR FAIRE FONCTIONNER LA CARTE !
  acf_update_setting('google_api_key', 'AIzaSyBOYwuRvoIVZinZmJeQrwEnHTz4mm53cK0');

	if( function_exists('acf_add_options_page') ) {

    acf_add_options_page(array(
			'page_title' 	=> __("Gérer les informations de l'entreprise", 'my_text_domain'),
			'menu_title' 	=> __('Informations', 'my_text_domain'),
			'menu_slug' 	=> 'infos',
      'position'    => 8,
      'icon_url'    => 'dashicons-editor-help',
		));

    acf_add_options_page(array(
			'page_title' 	=> __("Gérer le slider", 'my_text_domain'),
			'menu_title' 	=> __('Slider', 'my_text_domain'),
			'menu_slug' 	=> 'slider',
      'position'    => 9,
      'icon_url'    => 'dashicons-images-alt',
		));

    acf_add_options_page(array(
			'page_title' 	=> __("Gérer les réseaux sociaux", 'my_text_domain'),
			'menu_title' 	=> __('Réseaux sociaux', 'my_text_domain'),
			'menu_slug' 	=> 'social-media',
      'position'    => 9,
      'icon_url'    => 'dashicons-share',
		));

    acf_add_options_page(array(
			'page_title' 	=> __("Gérer la location", 'my_text_domain'),
			'menu_title' 	=> __('Location', 'my_text_domain'),
			'menu_slug' 	=> 'location',
      'position'    => 9,
      'icon_url'    => 'dashicons-location-alt',
		));


	}

}
add_action('acf/init', 'my_acf_init');





// PLUGINS REQUIS
require_once dirname(__FILE__) . '/inc/plugins/class-tgm-plugin-activation.php';
function enregistrer_mes_plugins_v2(){
	$plugins = array(

		//Acf
		array(
			'name'			=> 'ACF Wordpress',
			'slug'			=> 'ACF-wordpress-5.6.7',
			'source'		=> get_stylesheet_directory() . '/plugins/advanced-custom-fields-pro.5.6.7.zip',
			'required'	=> true,
		),

		//ACF RGBA picker
		array(
			'name' => 'ACF Rgba color picker', // Le nom du plugin
			'slug' => 'acf-rgba-color-picker', // Le slug du plugin (généralement le nom du dossier)
			'required' => true, // FALSE signifie que le plugin est seulement recommandé
		),

		//ACF Table field
		array(
			'name' => 'ACF Table Field', // Le nom du plugin
			'slug' => 'advanced-custom-fields-table-field', // Le slug du plugin (généralement le nom du dossier)
			'required' => true, // FALSE signifie que le plugin est seulement recommandé
		),

    // WP Google Fonts
		array(
			'name' => 'wp-google-fonts', // Le nom du plugin
			'slug' => 'wp-google-fonts', // Le slug du plugin (généralement le nom du dossier)
			'required' => true, // FALSE signifie que le plugin est seulement recommandé
		),

		// //Yoast
		// array(
		// 	'name' => 'Yoast Wordpress SEO', // Le nom du plugin
		// 	'slug' => 'wordpress-seo', // Le slug du plugin (généralement le nom du dossier)
		// 	'required' => true, // FALSE signifie que le plugin est seulement recommandé
		// ),

		//iTheme security
		array(
			'name' => 'iTheme security', // Le nom du plugin
			'slug' => 'better-wp-security', // Le slug du plugin (généralement le nom du dossier)
			'required' => true, // FALSE signifie que le plugin est seulement recommandé
		),

		// //Advanced Access Manager
		// array(
		// 	'name' => 'Advanced Access Manager', // Le nom du plugin
		// 	'slug' => 'advanced-access-manager', // Le slug du plugin (généralement le nom du dossier)
		// 	'required' => true, // FALSE signifie que le plugin est seulement recommandé
		// ),
    //
		// array(
		// 	'name' => 'Cookie Notice', // Le nom du plugin
		// 	'slug' => 'cookie-notice', // Le slug du plugin (généralement le nom du dossier)
		// 	'required' => true, // FALSE signifie que le plugin est seulement recommandé
		// ),


	);

		$theme_text_domain = 'Template_C-Full-Logo-Image'; // Changer pour le text-domain du theme

		$config = array(
		 'domain' => $theme_text_domain, // Text domain - le même que celui de votre thème
		 'default_path' => '', // Chemin absolu par défaut pour les plugins pré-packagés
		 'menu' => 'install-my-theme-plugins', // Menu slug
		 'strings' => array(
			 'page_title' => __( 'Installer les plugins recommandés', $theme_text_domain ), //
			 'menu_title' => __( 'Installation des Plugins', $theme_text_domain ), //
			 'instructions_install' => __( 'Le plugin %1$s est recommandé pour ce thème. Cliquer sur le boutton pour installer et activer %1$s.', $theme_text_domain ), // %1$s = nom du plugin
			 'instructions_activate' => __( 'Le plugin %1$s est installé mais inactif. Aller à <a href="%2$s">la page d administration</a> pour son activation.', $theme_text_domain ), // %1$s = nom du plugin, %2$s = plugins page URL
			 'button' => __( 'Installer %s maintenant', $theme_text_domain ), // %1$s = nom du plugin
			 'installing' => __( 'Installation du Plugin: %s', $theme_text_domain ), // %1$s = nom du plugin
			 'oops' => __( 'Une erreur s est produite.', $theme_text_domain ), //
			 'notice_can_install' => __( 'Ce thème recommande le plugin %1$s. <a href="%2$s"><strong>Cliquer ici pour commencer son installation</strong></a>.', $theme_text_domain ), // %1$s = nom du plugin , %2$s = TGMPA page URL
			 'notice_cannot_install' => __( 'Désolé, vous ne détenez pas les permissions necessaires pour installer le plugin %1$s.', $theme_text_domain ), // %1$s = nom du plugin
			 'notice_can_activate' => __( 'Ce thème necessite le plugin %1$s. Actuellement inactif, vous devez vous rendre sur <a href="%2$s">la page d administration du plugin</a> pour l activer.', $theme_text_domain ), // %1$s = plugin name, %2$s = plugins page URL
			 'notice_cannot_activate' => __( 'Désolé, vous ne détenez pas les permissions necessaires pour activer le plugin %1$s.', $theme_text_domain ), // %1$s = nom du plugin
			 'return' => __( 'Retour à l installeur de plugins', $theme_text_domain ),
		 ),
 		);
 tgmpa( $plugins, $config);

}
add_action( 'tgmpa_register', 'enregistrer_mes_plugins_v2' );





// COLORIZER

// Compile systématiquement le scss à chaque rafraîchissment de page/changement dans le scss.
if ( is_customize_preview() && !defined('WP_SCSS_ALWAYS_RECOMPILE')) {
	define('WP_SCSS_ALWAYS_RECOMPILE', true);
}

// Définit les couleurs par défaut des champs du sélecteur.
$default_colors = array(
	'color-header'	              => '#333333',
	'color-liens' 	              => 'blue',
  'color-liens-survol' 	        => 'darkblue',
  'color-burger' 	              => 'black',
  'color-burger-survol' 	      => 'blue',
  'color-info-m' 	              => 'black',
  'color-info-m-survol' 	      => 'blue',
  'color-liens-menu' 	          => 'blue',
  'color-liens-menu-survol' 	  => 'darkblue',
  'color-liens-social' 	        => 'blue',
  'color-liens-social-survol' 	=> 'darkblue',
  'color-informations' 	        => 'black',
  'color-téléphone' 	          => 'red',
  'color-contenu' 	            => 'whitesmoke',
  'color-texte' 	              => 'rgb(38, 38, 38)',
  'color-actions' 	            => 'blue',
  'color-actions-survol' 	      => 'darkblue',
  'color-actions-background' 	  => 'black',
  'color-preloader' 	          => 'white',
  'color-footer' 	              => '#333333',
);

// Update SCSS variables
function wp_scss_set_variables(){

    // Get the default colors
    global $default_colors;

    // Prepare the array
    $variables = array();

    // Loop through each variable and get theme_mod
    foreach( $default_colors as $key => $value ) {

    	$variables[$key] = get_theme_mod( $key, $value );

    }
	return $variables;

}
add_filter('wp_scss_variables', 'wp_scss_set_variables');


// Register settings and controls with the Customizer.
add_action( 'customize_register', 'starter_customizer_register' );
function starter_customizer_register() {

	global $wp_customize;
	global $default_colors;

	// Loop through each variable in the array
	foreach($default_colors as $key => $value) {

        // Add setting for each variable
		$wp_customize->add_setting( $key , array(
		    'default' => $value,
		    'sanitize_callback' => 'sanitize_hex_color',
		) );

        // Add control for each variable
		$wp_customize->add_control(
			new WP_Customize_Color_Control(
			$wp_customize,
			$key,
			array(
				'label'      => ucwords( str_replace( 'color-', '', $key ) ),
				'section'    => 'colors',
				'settings'   => $key,
			) )
		);
	}
}

?>
