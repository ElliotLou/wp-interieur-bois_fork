<?php
//Intégrer ces fonctions au fichier blocs.php
function add_bloc_vignettes($labels_info){

  //Variables =
  $couleurs_globales = get_field('couleurs_globales','option');
  $couleurs_vignettes = get_field('couleurs_bloc_vignettes', 'option');

  $labels_info = $labels_info['bloc_vignettes'];
  $bloc_options = $labels_info['options'];
    if($bloc_options && in_array('add_palmier', $bloc_options)):
      $add_palmier = true;
        $bandeau_palmier = $labels_info['bandeau_palmier'];
    else: $add_palmier = false;
    endif;

    if($bloc_options && in_array('remove_background', $bloc_options)):
      $on_bloc = false;
      $remove_background = true;
    else: $on_bloc = true; $remove_background = false;
    endif;

    if($bloc_options && in_array('add_shadow', $bloc_options)):
      $add_shadow = true;
    else: $add_shadow = false;
    endif;

    if($add_palmier):
      AddBandeauPalmier($bandeau_palmier, $remove_background);
      endif;
  ?>

  <section class="main-section bloc-vignettes container-fluid <?php if($on_bloc): echo ' on-bloc border-radius-all'; if($add_shadow): echo ' shadow-container'; endif; else: echo ' without-bloc'; endif; ?> ">
    <?php
    // On vérifie si il y a un titre et on récupères les paramètres
    if($bloc_options && in_array('add_titre', $bloc_options)):
      $add_titre = true;
      $titre_infos = $labels_info['titre'];
      $param_titre = $titre_infos['parametres_titre_bloc'];

    else : $add_titre = false;
    endif;

    if($add_titre):
      $vignette_has_titre = true;
      add_titre($param_titre, 'h2', 'header',$on_bloc, 'titre titre-bloc');
    else: $vignette_has_titre = false;
    endif;

    //On vérifie si il y a un paragraphe et on initialise la première section
    if($bloc_options && in_array('add_para', $bloc_options)):
      $paragraphe = $labels_info['paragraphe_before'];
      $para_infos = $paragraphe['paragraphe'];

      add_para($para_infos, $vignette_has_titre, false, null, $on_bloc);

    endif;

    //On récupère les info sur les vignettes et on instancie la section

    $vignettes = $labels_info['vignettes'];
    $back_vignettes = $vignettes['fond_vignettes_bloc_vignettes'];
    //Instanciation du container de vignette_has_titre
    ?>
      <!-- <section> -->
        <article class="vignettes-container container-fluid"
        <?php
        switch($back_vignettes):
          case 'couleur_defaut' :
            echo 'style=" background-color : '.$couleurs_globales['couleur_fond_principal'].';"';
            break;
          case 'couleur_change':
            echo 'style=" background-color : '.$vignettes['couleur_derriere_vignettes'].';"';
            break;
          case 'image' :
            echo 'style=" background : url('.$vignettes['image_derriere_vignettes']['url'].') center center fixed;
                  -webkit-background-size: cover;
                  -moz-background-size: cover;
                  -o-background-size: cover;
                  background-size: cover;
                "';
        endswitch;
        ?>
        >

        <?php
        $type_vignette = $vignettes['type_vignette'];
        switch($type_vignette):
          case 'textes':
            $vignettes_texte = $vignettes['vignettes_texte'];
            add_vignettes_textes($vignettes_texte);
          break;

          case 'image' :
            $vignette_image = $vignettes['vignettes_image'];
            $couleurs_vignette = get_field('couleurs_bloc_vignettes','option');
            add_vignettes_image_new($vignette_image);
          break;

          case 'titre_para':
            $vignettes_colonne = $vignettes['vignettes_colonne'];
            $options_vignette = $vignettes_colonne['options'];
            if($options_vignette):
              if(in_array('change_color', $options_vignette)):
                $fond_entete = $vignettes_colonne['fond_entete'];
                $textes_entete = $vignettes_colonne['textes_entete'];
                $fond_para = $vignettes_colonne['fond_para'];
                $textes_para = $vignettes_colonne['textes_para'];

              else:
                $couleurs_vignettes_colonne = $couleurs_vignettes['vignettes_colonne'];
                $fond_entete = $couleurs_vignettes_colonne['fond_entete'];
                $textes_entete = $couleurs_vignettes_colonne['texte_entete'];
                $fond_para = $couleurs_vignettes_colonne['fond_para'];
                $textes_para = $couleurs_vignettes_colonne['texte_para'];
              endif;

              if(in_array('border_radius', $options_vignette)):
                $bordered = true;
                $borders = $vignettes_colonne['coins_arrondies'];
              else: $bordered = false;
              endif;

              if(in_array('add_ombre', $options_vignette)):
                $shadow = true;
              else: $shadow = false;
              endif;
            endif;


            $vignettes = $vignettes_colonne['vignettes'];
            //initialisation du container
            ?>
            <div class="container-fluid bloc-center container-vcolonnes">
              <div class="row flex-center">
            <?php
              foreach($vignettes as $vignette):
                $opt_vignette = $vignette['options'];
                if($opt_vignette && in_array('add_link', $opt_vignette)):
                  $add_link = true;
                  $link = $vignette['lien'];
                else: $add_link = false;
                endif;

                $entete = $vignette['entete'];
                $paragraphe = $vignette['paragraphe'];
                //Initialisation de la vignette colonne
                ?>
                <div class="vignette-colonne col-xs-12 col-sm-6 col-md-3 col-lg-3 col-xlg-5">
                  <div class="inner-vignette <?php if($bordered): echo " ".$borders; endif; if($shadow): echo ' shadow-vignette'; endif;?>">
                    <div
                      class="entete-vignette"
                      style="background-color : <?php echo $fond_entete; ?> "
                    >
                      <?php
                        foreach($entete as $ent):
                          $ligne = $ent['ligne'];
                            $texte = $ligne['texte'];
                            $police = $ligne['option_police'];
                            $taille = $ligne['taille'];
                            ?>
                            <span
                              class="<?php echo $police; ?>"
                              style="font-size : <?php echo $taille; ?>px; color : <?php echo $textes_entete; ?>;"
                            >
                              <?php echo $texte; ?>
                            </span>
                            <?php
                        endforeach;
                      ?>
                    </div>
                    <div
                      class="para-vignette"
                      style="background-color : <?php echo $fond_para; ?>; color : <?php echo $textes_para; ?>"
                    >
                      <?php echo $paragraphe; ?>
                    </div>
                    <?php
                      if($add_link):
                        ?>
                        <div class="learn-more">
                          <a href=" <?php echo $link; ?> "> ... En savoir plus </a>
                        </div>
                        <?php
                      endif;
                    ?>
                  </div>
                </div>
                <?php
              endforeach;
              ?>
              </div>
            </div>
              <?php

          break;
        endswitch;

        //Bloc boutons
        if($bloc_options && in_array('add_bouton', $bloc_options)):
          $bouton_bloc = $labels_info['bouton'];
          $bouton = $bouton_bloc['parametre_bouton_vignette'];

          add_button($bouton, 'bouton-vignette');
        endif;
        ?>
      </article>
      <!-- </section> -->

    <?php

    //On vérifie si il y a un paragraphe et on initialise la première section
    if($bloc_options && in_array('add_para_after', $bloc_options)):
      $paragraphe = $labels_info['paragraphe_after'];
      $para_infos = $paragraphe['paragraphe'];

      add_para($para_infos, $vignette_has_titre, false, null, $on_bloc);

    endif;
    ?>
  </section>
  <?php
}

function add_vignettes_image_new($vignette_image){
  $couleurs_vignettes = get_field('couleurs_bloc_vignettes', 'option');

  $style_vignette_image = $vignette_image['style_vignette'];

  $vimage_options = $vignette_image['options'];
    if($vimage_options && in_array('change_color', $vimage_options)):
      $couleur_nav = $vignette_image['couleur_nav_icone'];
      $fond_vignettes = $vignette_image['fond_vignettes'];
      $couleur_labels_vignettes = $vignette_image['labels_vignettes'];

      else:
      $couleurs_vignettes_image = $couleurs_vignettes['vignettes_images'];

      $couleur_nav = $couleurs_vignettes_image['couleur_nav_icone'];
      $fond_vignettes = $couleurs_vignettes_image['couleur_fond_vignettes'];
      $couleur_labels_vignettes = $couleurs_vignettes_image['couleur_label'];
      endif;

    if($vimage_options && in_array('rect', $vimage_options)):
      $rect = true;
        $forme_rect = $vignette_image['forme_rectangle'];
      else:
      $rect = false;
      endif;

    if($vimage_options && in_array('add_ombre', $vimage_options)):
      $add_ombre = true;
      else: $add_ombre = false;
      endif;

    if($vimage_options && in_array('border_radius', $vimage_options)):
      $bordered = true;
      $borders = $vignette_image['coins_arrondies'];
      else: $bordered = false;
      endif;


  $vignettes = $vignette_image['vignettes'];
  $dispo_vignettes = $vignette_image['disposition_des_vignettes'];
  $nb_vignettes = $vignette_image['nb_vignettes'];


  $fond_label_cover_rgb = hex2rgb($fond_vignettes);
  $fond_label_cover_rgb = implode(",", $fond_label_cover_rgb);
  $fond_label_cover = 'rgba('.$fond_label_cover_rgb.', .6)';

  ?>


    <?php
      if($dispo_vignettes == 'one_line'):
        ?>
        <div class="slider bloc-center" data-slide="<?php echo $nb_vignettes;?>" data-slideold="<?php echo $nb_vignettes;?>" data-color="<?php echo $couleur_nav;?>">
         <?php
      else:
        ?>
        <div class="slider bloc-center multiligne" data-slide="<?php echo $nb_vignettes;?>" data-slideold="<?php echo $nb_vignettes;?>" data-color="<?php echo $couleur_nav;?>">
         <?php
      endif;

           foreach($vignettes as $vignette):

             $image = $vignette['image'];
             $options = $vignette['options'];

             if($options && in_array('add_link', $options)):
               $add_link = true;
             else: $add_link = false;
             endif;

             ?>

             <div class="vignette">
                <div class=" <?php if($rect): echo $forme_rect; else: echo 'item-carre'; endif; ?>">
                  <?php if($add_link): echo '<a class="link-vignette" href="'.$vignette['lien'].'" target="_blank">'; endif; ?>

                    <?php
                    switch ($style_vignette_image):
                      case 'img_cover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                        </div>
                        <?php
                        break;
                      case 'img_cover_texte_hover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <?php
                          $textes_hover = $vignette['text_hover'];
                          ?>
                          <div class="text-hover-full"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                              foreach ($textes_hover as $lignes):
                                $ligne = $lignes['ligne'];
                                  $texte = $ligne['texte'];
                                  $option_police = $ligne['option_police'];
                                  $option = $ligne['option'];
                                    if($option && in_array('change_size', $option)):
                                      $change_size = true;
                                      $taille = $ligne['taille'];
                                      else: $change_size = false;
                                      endif;

                                ?>
                                <p class="<?php echo $option_police; ?>"
                                    style="
                                      color : <?php echo $couleur_labels_vignettes; ?>;
                                      <?php if($change_size): echo 'font-size : '.$taille.'px;'; endif; ?>
                                    "
                                >
                                <?php echo $texte; ?>
                                </p>
                                <?php
                                endforeach;
                            ?>
                          </div>
                        </div>
                        <?php
                        break;
                      case 'img_cover_zoom_hover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="zoom-hover-full"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <i class="fa fa-search-plus fa-4x" aria-hidden="true" style="color : <?php echo $couleur_labels_vignettes;?> "></i>
                          </div>
                        </div>
                        <?php
                        break;

                      case 'img_cover_et_label':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="label-top"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                              $label = $vignette['label'];
                            ?>
                            <p class="police-bolder"
                              style="color : <?php echo $couleur_labels_vignettes; ?>"
                            >
                              <?php echo $label; ?>
                            </p>
                          </div>

                        </div>
                        <?php
                        break;
                      case 'img_cover_et_label_texte_hover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="label-top"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                              $label = $vignette['label'];
                            ?>
                            <p class="police-bolder"
                              style="color : <?php echo $couleur_labels_vignettes; ?>"
                            >
                              <?php echo $label; ?>
                            </p>
                          </div>
                          <div class="text-more-hover"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                            $textes_hover = $vignette['text_hover'];
                            foreach ($textes_hover as $lignes):
                              $ligne = $lignes['ligne'];
                                $texte = $ligne['texte'];
                                $option_police = $ligne['option_police'];
                                $option = $ligne['option'];
                                  if($option && in_array('change_size', $option)):
                                    $change_size = true;
                                    $taille = $ligne['taille'];
                                    else: $change_size = false;
                                    endif;

                              ?>
                              <p class="<?php echo $option_police; ?>"
                                  style="
                                    color : <?php echo $couleur_labels_vignettes; ?>;
                                    <?php if($change_size): echo 'font-size : '.$taille.'px;'; endif; ?>
                                  "
                              >
                              <?php echo $texte; ?>
                              </p>
                              <?php
                              endforeach;
                            ?>
                          </div>

                        </div>
                        <?php
                        break;
                      case 'img_cover_et_label_zoom_hover':
                        ?>
                        <div class="slide-content cover <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="label-top"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <?php
                              $label = $vignette['label'];
                            ?>
                            <p class="police-bolder"
                              style="color : <?php echo $couleur_labels_vignettes; ?>"
                            >
                              <?php echo $label; ?>
                            </p>
                          </div>

                          <div class="zoom-hover-more"
                            <?php echo 'style="background-color : '.$fond_label_cover.';"'; ?>
                          >
                            <i class="fa fa-search-plus fa-4x" aria-hidden="true" style="color : <?php echo $couleur_labels_vignettes;?> "></i>
                          </div>

                        </div>
                        <?php
                        break;
                      case 'img_et_label':
                        ?>
                        <div class="slide-content <?php if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?> "
                          style="background-color : <?php echo $fond_vignettes;?>;"
                          >

                          <div class="img-container-vignette">
                            <div class="img-content">
                              <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                            </div>
                          </div>

                          <div class="label-container-under">
                            <p class="police-bolder"
                              style="color : <?php echo $couleur_labels_vignettes;?>"
                            >
                              <?php echo $vignette['label']; ?>
                            </p>
                          </div>

                        </div>
                        <?php
                        break;
                      default:
                        # code...
                        break;
                    endswitch;
                    ?>

                    <!-- <div class="slide-content<?php if($add_label): echo ' with-label'; endif; if($cover): echo " cover"; endif; if($add_ombre): echo " shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; if($with_zoom): echo ' with-zoom'; endif;?>"
                         style="background-color : <?php echo $fond_vignettes;?>;"
                      >
                      <div class="img-container-vignette">
                        <div class="img-content">
                          <img src="<?php if($image['sizes']['medium'] && $nb_vignettes > 3): echo $image['sizes']['medium']; else: echo $image['url']; endif; ?>" alt="<?php echo $image['alt']; ?>">
                        </div>
                      </div>
                      <?php
                      if($add_label || $with_zoom):
                        ?>
                        <div class="label-container <?php if($add_link && $cover): echo 'covering'; endif; if($with_text):echo ' with-text'; endif;?>"
                          <?php if($with_zoom): echo 'style="background-color : '.$fond_label_cover.';"';endif; ?>
                          >
                          <?php

                          if($with_zoom):
                            ?>
                            <i class="fa fa-search-plus" aria-hidden="true" style="color : <?php echo $couleur_labels_vignettes;?> "></i>
                            <?php
                          elseif($add_label && $label_hover):
                            ?>
                            <p class="justify-center">
                              <?php echo $vignette['label']; ?>
                            </p>
                            <?php
                          else:
                            ?>
                            <p class="label-vignette " style="<?php if($cover): echo '; background-color : '.$fond_label_cover.';'; else: echo ';'; endif;?>">
                              <span class="label-perso"
                                style="color : <?php echo $couleur_labels_vignettes;?>"
                              >
                                <?php echo $vignette['label']; ?>
                              </span>
                            </p>
                            <?php
                            if($with_text):
                              $text_to_display = $vignette['text_hover'];
                              ?>
                              <div class="label-more" style="background-color : <?php echo $fond_label_cover; ?>; color : <?php echo $couleur_labels_vignettes;?>; ">
                                <?php
                                foreach($text_to_display as $text):
                                  $ligne = $text['ligne'];
                                  $texte = $ligne['texte'];
                                  $option_police = $ligne['option_police'];
                                  $options = $ligne['options'];
                                    if($options && in_array('change_size', $options)):
                                      $change_size = true;
                                      $size = $text['taille'];
                                    else : $change_size = false;
                                    endif;
                                    ?>
                                    <p class="<?php echo $option_police; ?>"
                                      <?php if($change_size): echo 'style="font-size : '.$size.'";'; endif; ?>
                                    >
                                      <?php echo $texte; ?>
                                    </p>
                                    <?php
                                endforeach;
                                ?>
                              </div>
                              <?php
                            endif;
                          endif;
                          ?>



                        </div>

                        <?php
                      endif;
                      ?>
                    </div> -->

                  <?php
                  if($add_link): echo '</a>'; endif;
              ?>
              </div>
            </div>
              <?php
           endforeach;
         ?>
       </div>
        <?php
}

function add_vignettes_textes($vignettes_texte){
  $couleurs_vignettes = get_field('couleurs_bloc_vignettes','option');
    ?>
    <div class="container-fluid bloc-center container-vtextes">
      <div class="row flex-center">

    <?php
      $vtexte_options = $vignettes_texte['options'];
      if($vtexte_options):
        if(in_array('add_ombre', $vtexte_options)):
          $add_ombre = true;
        else: $add_ombre = false;
        endif;

        if(in_array('change_color',$vtexte_options)):
          $couleur_labels_vignettes = $vignettes_texte['labels_vignettes'];
          $fond_vignettes = $vignettes_texte['fond_vignettes'];
          $fond_vignettes_hover = $vignettes_texte['fond_vignettes_hover'];
          $couleur_labels_hover = $vignettes_texte['labels_vignettes_hover'];
        else:
          $couleurs_vignettes_texte = $couleurs_vignettes['vignettes_textes'];
            $couleur_labels_vignettes = $couleurs_vignettes_texte['couleur_texte'];
            $fond_vignettes = $couleurs_vignettes_texte['couleur_fond'];
            $fond_vignettes_hover = $couleurs_vignettes_texte['couleur_fond_hover'];
            $couleur_labels_hover = $couleurs_vignettes_texte['couleur_texte_hover'];
        endif;

        if(in_array('border_radius', $vtexte_options)):
          $bordered = true;
          $borders = $vignettes_texte['coins_arrondies'];
        else: $bordered = false;
        endif;
      endif;


    $vtexte_vignettes = $vignettes_texte['vignettes'];
          if($vtexte_vignettes):
            foreach($vtexte_vignettes as $vignette):
              $label = $vignette['label'];
              $options = $vignette['options'];
              if($options && in_array('add_link',$options)):
                $add_link = true;
              else: $add_link = false;
              endif;
              ?>
              <div class=" col-xs-12 col-sm-6 col-md-4 col-lg-3"
              >
                   <?php
                   if($add_link):
                     echo '
                      <a class="vignette-texte ';
                      if($add_ombre): echo 'shadow ';endif; if($bordered): echo ' '.$borders.' '; endif;
                      echo 'link-vignette" href="'.$vignette['lien'].'" target="_blank"
                        style="color : '.$couleur_labels_vignettes.'; background-color : '.$fond_vignettes.'; "
                        onmouseover="this.style.backgroundColor = \''.$fond_vignettes_hover.'\'; this.style.color = \''.$couleur_labels_hover.'\'"
                        onmouseout="this.style.backgroundColor = \''.$fond_vignettes.'\'; this.style.color = \''.$couleur_labels_vignettes.'\'"
                      >';
                      foreach($label as $ligne):
                        ?>
                        <span>
                          <?php echo $ligne['ligne']; ?>
                        </span>
                        <?php
                      endforeach;
                    echo' </a>
                      ';
                   else:
                     ?>
                       <div class="vignette-texte <?php if($add_ombre): echo "shadow-vignette"; endif; if($bordered): echo ' '.$borders; endif; ?>"
                            style="background-color : <?php echo $fond_vignettes;?>;">

                             <?php
                             foreach($label as $ligne):
                               ?>
                               <span style="color : <?php echo $couleur_labels_vignettes; ?>; ">
                                 <?php echo $ligne['ligne']; ?>
                               </span>
                               <?php
                             endforeach;
                             ?>
                       </div>
                           <?php

                   endif;
               echo '</div>';

            endforeach;
          endif;
          ?>
        </div>
      </div>
          <?php
}

?>
